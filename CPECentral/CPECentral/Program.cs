﻿#region Using directives

using System;
using System.Diagnostics;
using System.Drawing;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using CPECentral.Data.EF5;
using CPECentral.Dialogs;
using CPECentral.QMS;
using NcCommunicator.Data;
using NcCommunicator.Data.Model;
using nGenLibrary.Security;

#endregion

namespace CPECentral
{
    internal static class Program
    {
        private const int SW_RESTORE = 9;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern Boolean ShowWindow(IntPtr hWnd, Int32 nCmdShow);

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            bool createdNew = true;

            //string commonAppDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

            //var db = new MachinesDataProvider(commonAppDir);

            //var serialMachine = new SerialMachine();
            //serialMachine.Name = "Hardinge T42";
            //serialMachine.ComPort = "COM3";
            //serialMachine.StopBits = StopBits.Two;
            //serialMachine.Parity = Parity.Even;
            //serialMachine.Handshake = Handshake.XOnXOff;
            //serialMachine.DataBits = 7;
            //serialMachine.BaudRate = 9600;
            //serialMachine.DtrEnable = true;
            //serialMachine.RtsEnable = true;
            //serialMachine.SendXOnChar = 17;
            //serialMachine.SendXOffChar = 19;
            //serialMachine.ReceiveXOnChar = 18;
            //serialMachine.ReceiveXOffChar = 20;
            //serialMachine.ProgramStart = "%";
            //serialMachine.ProgramEnd = "M30";
            //serialMachine.NewLine = @"\r\n\n";
            //serialMachine.Photo = null;

            //db.Insert(serialMachine);

            //serialMachine.Name = "Hardinge T42-A";
            //serialMachine.ComPort = "COM4";

            //db.Insert(serialMachine);

            //serialMachine.Name = "Hardinge C42";
            //serialMachine.ComPort = "COM3";

            //db.Insert(serialMachine);

            using (var mutex = new Mutex(true, "CPECentral", out createdNew)) {
                if (createdNew) {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

                    Session.Initialize();

                    Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                    Application.ThreadException += Application_ThreadException;

                    var form = new MainForm();

                    Application.Run(form);
                }
                else {
                    Process current = Process.GetCurrentProcess();
                    foreach (Process process in Process.GetProcessesByName(current.ProcessName)) {
                        if (process.Id != current.Id) {
                            ShowWindow(process.MainWindowHandle, SW_RESTORE);
                            SetForegroundWindow(process.MainWindowHandle);
                            break;
                        }
                    }
                }
            }
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            string msg = e.Exception.InnerException == null ? e.Exception.Message : e.Exception.InnerException.Message;

            MessageBox.Show(msg);
        }

        private static void EnsureThereIsAnAdminAccount()
        {
            try {
                using (var cpe = new CPEUnitOfWork()) {
                    EmployeeGroup adminGroup = cpe.EmployeeGroups.GetByName("BUILTIN_ADMIN_GROUP");

                    if (adminGroup == null) {
                        adminGroup = new EmployeeGroup();
                        adminGroup.Name = "BUILTIN_ADMIN_GROUP";
                        adminGroup.GrantPermission(AppPermission.Administrator);
                        cpe.EmployeeGroups.Add(adminGroup);
                        cpe.Commit();
                    }

                    Employee adminEmployee = cpe.Employees.GetByUserName("admin");

                    if (adminEmployee == null) {
                        adminEmployee = new Employee();
                        adminEmployee.FirstName = "System";
                        adminEmployee.LastName = "Administrator";
                        adminEmployee.UserName = "admin";
                        adminEmployee.EmployeeGroupId = adminGroup.Id;

                        Password securedPassword = Session.GetInstanceOf<IPasswordService>().SecurePassword("5jc3ngltd");

                        adminEmployee.Password = securedPassword.Hash;
                        adminEmployee.Salt = securedPassword.Salt;

                        cpe.Employees.Add(adminEmployee);

                        cpe.Commit();
                    }
                }
            }
            catch (Exception ex) {
                string msg = ex.InnerException == null ? ex.Message : ex.InnerException.Message;

                MessageBox.Show(msg);
            }
        }

        private static void AddMyAccount()
        {
            try {
                using (var cpe = new CPEUnitOfWork()) {
                    EmployeeGroup myGroup = cpe.EmployeeGroups.GetByName("Power users");

                    if (myGroup == null) {
                        myGroup = new EmployeeGroup();
                        myGroup.Name = "Power users";
                        myGroup.GrantPermission(AppPermission.ManageDocuments);
                        myGroup.GrantPermission(AppPermission.ManageEmployees);
                        myGroup.GrantPermission(AppPermission.ManageOperations);
                        myGroup.GrantPermission(AppPermission.ManageParts);
                        myGroup.GrantPermission(AppPermission.EditSettings);

                        cpe.EmployeeGroups.Add(myGroup);

                        cpe.Commit();
                    }

                    Employee myEmployee = cpe.Employees.GetByUserName("adam");

                    if (myEmployee == null) {
                        myEmployee = new Employee();
                        myEmployee.FirstName = "Adam";
                        myEmployee.LastName = "Tunbridge";
                        myEmployee.UserName = "adam";
                        myEmployee.EmployeeGroupId = myGroup.Id;

                        Password securedPassword = Session.GetInstanceOf<IPasswordService>().SecurePassword("password");

                        myEmployee.Password = securedPassword.Hash;
                        myEmployee.Salt = securedPassword.Salt;

                        cpe.Employees.Add(myEmployee);

                        cpe.Commit();
                    }
                }
            }
            catch (Exception ex) {
                string msg = ex.InnerException == null ? ex.Message : ex.InnerException.Message;

                MessageBox.Show(msg);
            }
        }
    }
}