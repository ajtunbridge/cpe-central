﻿namespace CPECentral.Views
{
    partial class StartPageView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.turnoverTargetView1 = new CPECentral.Views.StartPageTargetView();
            this.engineerToolsView1 = new CPECentral.Views.StartPageCalculatorSelectView();
            this.startPageFindToolBoxView1 = new CPECentral.Views.StartPageFindToolBoxView();
            this.checkStockLevelsView1 = new CPECentral.Views.StartPageCheckStockView();
            this.startPageUserInfoView1 = new CPECentral.Views.StartPageUserInfoView();
            this.SuspendLayout();
            // 
            // turnoverTargetView1
            // 
            this.turnoverTargetView1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.turnoverTargetView1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.turnoverTargetView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.turnoverTargetView1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.turnoverTargetView1.Location = new System.Drawing.Point(682, 10);
            this.turnoverTargetView1.Margin = new System.Windows.Forms.Padding(10);
            this.turnoverTargetView1.MaximumSize = new System.Drawing.Size(800, 185);
            this.turnoverTargetView1.MinimumSize = new System.Drawing.Size(275, 185);
            this.turnoverTargetView1.Name = "turnoverTargetView1";
            this.turnoverTargetView1.Size = new System.Drawing.Size(334, 185);
            this.turnoverTargetView1.TabIndex = 4;
            // 
            // engineerToolsView1
            // 
            this.engineerToolsView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.engineerToolsView1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.engineerToolsView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.engineerToolsView1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.engineerToolsView1.Location = new System.Drawing.Point(682, 450);
            this.engineerToolsView1.Margin = new System.Windows.Forms.Padding(10);
            this.engineerToolsView1.Name = "engineerToolsView1";
            this.engineerToolsView1.Size = new System.Drawing.Size(334, 347);
            this.engineerToolsView1.TabIndex = 3;
            // 
            // startPageFindToolBoxView1
            // 
            this.startPageFindToolBoxView1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startPageFindToolBoxView1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.startPageFindToolBoxView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.startPageFindToolBoxView1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startPageFindToolBoxView1.Location = new System.Drawing.Point(682, 215);
            this.startPageFindToolBoxView1.Margin = new System.Windows.Forms.Padding(10);
            this.startPageFindToolBoxView1.MaximumSize = new System.Drawing.Size(800, 215);
            this.startPageFindToolBoxView1.MinimumSize = new System.Drawing.Size(275, 215);
            this.startPageFindToolBoxView1.Name = "startPageFindToolBoxView1";
            this.startPageFindToolBoxView1.Size = new System.Drawing.Size(334, 215);
            this.startPageFindToolBoxView1.TabIndex = 2;
            // 
            // checkStockLevelsView1
            // 
            this.checkStockLevelsView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkStockLevelsView1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.checkStockLevelsView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkStockLevelsView1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkStockLevelsView1.Location = new System.Drawing.Point(10, 327);
            this.checkStockLevelsView1.Margin = new System.Windows.Forms.Padding(10);
            this.checkStockLevelsView1.Name = "checkStockLevelsView1";
            this.checkStockLevelsView1.Size = new System.Drawing.Size(652, 470);
            this.checkStockLevelsView1.TabIndex = 1;
            // 
            // startPageUserInfoView1
            // 
            this.startPageUserInfoView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.startPageUserInfoView1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.startPageUserInfoView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.startPageUserInfoView1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startPageUserInfoView1.Location = new System.Drawing.Point(10, 10);
            this.startPageUserInfoView1.Margin = new System.Windows.Forms.Padding(10);
            this.startPageUserInfoView1.Name = "startPageUserInfoView1";
            this.startPageUserInfoView1.Size = new System.Drawing.Size(652, 297);
            this.startPageUserInfoView1.TabIndex = 0;
            // 
            // StartPageView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.turnoverTargetView1);
            this.Controls.Add(this.engineerToolsView1);
            this.Controls.Add(this.startPageFindToolBoxView1);
            this.Controls.Add(this.checkStockLevelsView1);
            this.Controls.Add(this.startPageUserInfoView1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "StartPageView";
            this.Size = new System.Drawing.Size(1026, 807);
            this.ResumeLayout(false);

        }

        #endregion

        private StartPageUserInfoView startPageUserInfoView1;
        private StartPageCheckStockView checkStockLevelsView1;
        private StartPageFindToolBoxView startPageFindToolBoxView1;
        private StartPageCalculatorSelectView engineerToolsView1;
        private StartPageTargetView turnoverTargetView1;





    }
}
